import 'package:alphazone_shop/screen/auth_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class UnauthorizedScreen extends StatefulWidget {
  const UnauthorizedScreen({Key? key}) : super(key: key);

  @override
  _UnauthorizedScreenState createState() => _UnauthorizedScreenState();
}

class _UnauthorizedScreenState extends State<UnauthorizedScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    SchedulerBinding.instance?.addPostFrameCallback((timeStamp) async {
      Navigator.of(context).pushNamed(AuthScreen.routeName);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
