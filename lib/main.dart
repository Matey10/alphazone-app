import 'package:alphazone_shop/provider/auth.dart';
import 'package:alphazone_shop/provider/cart.dart';
import 'package:alphazone_shop/provider/order.dart';
import 'package:alphazone_shop/provider/product_list.dart';
import 'package:alphazone_shop/screen/auth_screen.dart';
import 'package:alphazone_shop/screen/cart.dart';
import 'package:alphazone_shop/screen/edit_product.dart';
import 'package:alphazone_shop/screen/order.dart';
import 'package:alphazone_shop/screen/product_detail.dart';
import 'package:alphazone_shop/screen/product_overview.dart';
import 'package:alphazone_shop/screen/unauthorized_screen.dart';
import 'package:alphazone_shop/screen/user_product.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

_protectedRoute(Widget screen) {
  return (BuildContext ctx) {
    if (!Provider.of<AuthProvider>(ctx).isAuth) {
      return UnauthorizedScreen();
    }

    return screen;
  };
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider.value(value: AuthProvider()),
          ChangeNotifierProxyProvider<AuthProvider, ProductListProvider>(
              create: (ctx) => ProductListProvider(),
              update: (ctx, auth, productList) {
                return ProductListProvider(
                  token: auth.token,
                  itemList: productList!.itemList,
                  onUnauthorized: auth.handleUnauthorized,
                  favoriteItemIdList: productList.favoriteItemIdList,
                );
              }),
          ChangeNotifierProxyProvider<AuthProvider, OrderProvider>(
              create: (ctx) => OrderProvider(token: '', orderList: []),
              update: (ctx, auth, prevProvider) {
                return OrderProvider(
                  token: auth.token,
                  orderList: prevProvider == null ? [] : prevProvider.orderList,
                  onUnauthorized: auth.handleUnauthorized,
                );
              }
          ),
          ChangeNotifierProvider(create: (ctx) => CartProvider()),
        ],
        child: Consumer<AuthProvider>(
          builder: (context, auth, _) {
            return MaterialApp(
              title: 'Alphazone Shop',
              theme: ThemeData(
                primaryColor: Color.fromRGBO(147, 216, 249, 1),
                accentColor: Color.fromRGBO(249, 180, 147, 1),
                fontFamily: "Lato",
              ),
              home: auth.isAuth ? ProductOverviewScreen() : AuthScreen(),
              routes: {
                AuthScreen.routeName: (ctx) => AuthScreen(),
                ProductOverviewScreen.routeName: (ctx) => ProductOverviewScreen(),
                ProductDetailScreen.routeName: (ctx) => ProductDetailScreen(),
                CartScreen.routeName: (ctx) => CartScreen(),
                OrderScreen.routeName: (ctx) => OrderScreen(),
                UserProductScreen.routeName: (ctx) => UserProductScreen(),
                EditProductScreen.routeName: (ctx) => EditProductScreen(),
              },
            );
          },
        ));
  }
}
