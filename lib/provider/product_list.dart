import 'package:alphazone_shop/api/product.dart' as api;
import 'package:alphazone_shop/model/http_exception.dart';
import 'package:alphazone_shop/provider/product.dart';
import 'package:alphazone_shop/utils/auth.dart';
import 'package:flutter/material.dart';

class ProductListProvider with ChangeNotifier {
  final String token;
  final UnauthorizedHandler onUnauthorized;
  List<ProductProvider> _itemList;
  List<int> favoriteItemIdList;

  ProductListProvider(
      {token,
      List<ProductProvider>? itemList,
      onUnauthorized,
      favoriteItemIdList})
      : this.token = token == null ? '' : token,
        this._itemList = itemList == null ? [] : itemList,
        this.onUnauthorized =
            onUnauthorized == null ? defaultUnauthorized : onUnauthorized,
        this.favoriteItemIdList =
            favoriteItemIdList == null ? [] : favoriteItemIdList;

  Future<void> fetchProductList({ bool onlyUserProducts = false }) async {
    try {
      final resp = await Future.wait([
        onlyUserProducts ? api.getUserProductList(token: token) : api.getProductList(), 
        api.getFavoriteProductIdList(token)
      ]);

      // var list = await api.getProductList();

      _itemList = (resp[0] as List<ProductProvider>);
      favoriteItemIdList = (resp[1] as List<int>);
    } catch (error) {}

    notifyListeners();
  }

  Future toggleFavorite (int id) async {
    print('$id');
    try {
      if (favoriteItemIdList.indexOf(id) != -1) {
        await api.unfavoriteProduct(id: id, token: token);
      } else {
        await api.favoriteProduct(id: id, token: token);
      }

      favoriteItemIdList = await api.getFavoriteProductIdList(token);

      notifyListeners();
    } catch (error) {
      print('$error');
    }
  }

  List<ProductProvider> get itemList {
    return [..._itemList];
  }

  List<ProductProvider> get favoriteItemList {
    return _itemList.where((element) => (favoriteItemIdList.indexOf(element.id) != -1)).toList();
  }

  ProductProvider findById(int id) {
    return _itemList.firstWhere((element) => element.id == id);
  }

  Future addProduct({required ProductProvider product}) {
    final newProduct = ProductProvider(
      id: 0,
      title: product.title,
      price: product.price,
      description: product.description,
      imageUrl: product.imageUrl,
    );

    return api.createProduct(product: newProduct, token: token).then((product) {
      _itemList.add(product);
      notifyListeners();
    }).catchError(onUnauthorized);
  }

  Future updateProduct({required ProductProvider product}) {
    return api
        .updateProduct(
      product: product,
      token: token,
    )
        .then((product) {
      final prodIndex = _itemList.indexWhere((prod) => prod.id == product.id);

      if (prodIndex >= 0) {
        _itemList[prodIndex] = product;
        notifyListeners();
      }
    }).catchError(onUnauthorized);
  }

  Future deleteProduct({required int id}) {
    final elementIndex = _itemList.indexWhere((element) => element.id == id);
    final element = _itemList[elementIndex];
    _itemList.removeWhere((element) => element.id == id);
    notifyListeners();

    return api.deleteProduct(id: id, token: token).catchError((error) {
      _itemList.insert(elementIndex, element);
      notifyListeners();
      return Future.error(error);
    }).catchError(onUnauthorized);
  }

// void showFavoriteOnly() {
//   _showFavoriteOnly = true;
//   notifyListeners();
// }
//
// void showAll() {
//   _showFavoriteOnly = false;
//   notifyListeners();
// }
}
