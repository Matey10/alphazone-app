import 'package:alphazone_shop/model/http_exception.dart';
import 'package:alphazone_shop/provider/auth.dart';
import 'package:alphazone_shop/provider/product_list.dart';
import 'package:alphazone_shop/screen/edit_product.dart';
import 'package:alphazone_shop/utils/auth.dart';
import 'package:alphazone_shop/widget/app_drawer.dart';
import 'package:alphazone_shop/widget/user_product_list_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class UserProductScreen extends StatelessWidget {
  static const routeName = "/user-product";

  const UserProductScreen({Key? key}) : super(key: key);

  handleErr(BuildContext context, err) {
    return showDialog<Null>(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text('An error occurred'),
        content: Text(err.toString()),
        actions: [
          TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Okay'))
        ],
      ),
    );
  }

  void deleteHandler(
      BuildContext context, ProductListProvider pData, int id) async {
    try {
      await pData.deleteProduct(id: id);
    } catch (error) {
      handleErr(context, error);
    }
  }

  Future<void> _refreshProducts(BuildContext context) {
    return Provider.of<ProductListProvider>(context, listen: false)
        .fetchProductList(onlyUserProducts: true);
  }

  @override
  Widget build(BuildContext context) {
    // final pData = Provider.of<ProductListProvider>(context);

    return Scaffold(
        appBar: AppBar(
          title: const Text('Your Products'),
          actions: [
            IconButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(EditProductScreen.routeName);
                },
                icon: Icon(Icons.add))
          ],
        ),
        drawer: AppDrawer(),
        body: FutureBuilder(
          future: _refreshProducts(context),
          builder: (ctx, snapshot) => snapshot.connectionState ==
                  ConnectionState.waiting
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : RefreshIndicator(
                  onRefresh: () => _refreshProducts(ctx),
                  child: Consumer<ProductListProvider>(
                    builder: (ctx, pData, _) => Padding(
                      padding: EdgeInsets.all(8),
                      child: ListView.builder(
                        itemCount: pData.itemList.length,
                        itemBuilder: (ctx, index) {
                          return Column(
                            children: [
                              UserProductListItem(
                                product: pData.itemList[index],
                                onDelete: (id) {
                                  deleteHandler(context, pData, id);
                                },
                              ),
                              Divider(),
                            ],
                          );
                        },
                      ),
                    ),
                  ),
                ),
        ));
  }
}
