import 'package:alphazone_shop/provider/product_list.dart';
import 'package:alphazone_shop/widget/product_grid_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProductGrid extends StatelessWidget {
  final bool onlyFavorite;

  const ProductGrid({Key? key, required this.onlyFavorite}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final productData = Provider.of<ProductListProvider>(context);
    final favoriteItemIdList = productData.favoriteItemIdList;
    final productList =
        onlyFavorite ? productData.favoriteItemList : productData.itemList;

    return GridView.builder(
        padding: const EdgeInsets.all(10),
        itemCount: productList.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 10,
            mainAxisSpacing: 10),
        itemBuilder: (ctx, i) {
          return ChangeNotifierProvider.value(
            value: productList[i],
            child: ProductGridItem(
              isFavorite: favoriteItemIdList.indexOf(productList[i].id) > -1,
            ),
          );
        });
  }
}
