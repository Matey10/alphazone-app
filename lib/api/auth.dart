import 'dart:convert';
import 'dart:io';

import 'package:alphazone_shop/api/main.dart';
import 'package:alphazone_shop/dto/auth.dart';
import 'package:alphazone_shop/model/http_exception.dart';
import 'package:http/http.dart';

Future signUp(String email, String password) {
  final url = Uri.http(baseUrl, '/auth/sign-up');

  return post(
    url,
    headers: headers,
    body: jsonEncode({"email": email, "password": password}),
  ).then(handleHttpError).then((_) => null);
}

Future<LoginResponse> login(String email, String password) {
  final url = Uri.http(baseUrl, '/auth/login');

  return post(
    url,
    headers: headers,
    body: jsonEncode({"email": email, "password": password}),
  )
      .then(handleHttpError)
      .then((resp) => LoginResponse.fromJson(jsonDecode(resp.body)));
}

Future<void> logout(String token) {
  final url = Uri.http(baseUrl, '/logout');

  return post(
    url,
    headers: getHeadersWithAuth(token),
  ).then(handleHttpError);
}
