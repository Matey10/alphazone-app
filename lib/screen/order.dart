import 'package:alphazone_shop/provider/order.dart';
import 'package:alphazone_shop/widget/app_drawer.dart';
import 'package:alphazone_shop/widget/order_list_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';

/*
 * StatefulWidget handling
 */
// class OrderScreen extends StatefulWidget {
//   static const routeName = '/orders';
//
//   const OrderScreen({Key? key}) : super(key: key);
//
//   @override
//   _OrderScreenState createState() => _OrderScreenState();
// }
//
// class _OrderScreenState extends State<OrderScreen> {
//   bool _isLoading = false;
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     SchedulerBinding.instance?.addPostFrameCallback((timeStamp) {
//       setState(() {
//         _isLoading = true;
//       });
//       Provider.of<OrderProvider>(context, listen: false)
//           .fetchOrderList()
//           .then((value) {
//         setState(() {
//           _isLoading = false;
//         });
//       }).catchError((err) {
//         setState(() {
//           _isLoading = false;
//         });
//         throw err;
//       });
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: Text('Your Orders')),
//       drawer: AppDrawer(),
//       body: FutureBuilder(
//         future:
//             Provider.of<OrderProvider>(context, listen: false).fetchOrderList(),
//         builder: (ctx, dataSnapshot) {
//           if (dataSnapshot.connectionState == ConnectionState.waiting) {
//             return Center(child: CircularProgressIndicator());
//           }
//
//           if (dataSnapshot.error != null) {
//             // TODO: Handle error properly
//             return Center();
//           }
//
//           return Consumer<OrderProvider>(
//               builder: (ctx, orderData, child) => ListView.builder(
//                   itemBuilder: (ctx, i) {
//                     return OrderListItem(order: orderData.orderList[i]);
//                   },
//                   itemCount: orderData.orderList.length,
//               ));
//         },
//       ),
//     );
//   }
// }


/*
 * FutureBuilder Basics
 */
// class OrderScreen extends StatelessWidget {
//   static const routeName = '/orders';
//   const OrderScreen({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: Text('Your Orders')),
//       drawer: AppDrawer(),
//       body: FutureBuilder(
//         future:
//         Provider.of<OrderProvider>(context, listen: false).fetchOrderList(),
//         builder: (ctx, dataSnapshot) {
//           if (dataSnapshot.connectionState == ConnectionState.waiting) {
//             return Center(child: CircularProgressIndicator());
//           }
//
//           if (dataSnapshot.error != null) {
//             // TODO: Handle error properly
//             return Center();
//           }
//
//           return Consumer<OrderProvider>(
//               builder: (ctx, orderData, child) => ListView.builder(
//                 itemBuilder: (ctx, i) {
//                   return OrderListItem(order: orderData.orderList[i]);
//                 },
//                 itemCount: orderData.orderList.length,
//               ));
//         },
//       ),
//     );
//   }
// }

/*
 * FutureBuilder with cached future Basics
 */
class OrderScreen extends StatefulWidget {
  static const routeName = '/orders';
  const OrderScreen({Key? key}) : super(key: key);

  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  late Future _orderListFuture;

  Future _getOrderListFuture() {
    return Provider.of<OrderProvider>(context, listen: false).fetchOrderList();
  }

  @override
  void initState() {
    // TODO: implement initState
    _orderListFuture = _getOrderListFuture();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Your Orders')),
      drawer: AppDrawer(),
      body: FutureBuilder(
        future: _orderListFuture,
        builder: (ctx, dataSnapshot) {
          if (dataSnapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          }

          if (dataSnapshot.error != null) {
            // TODO: Handle error properly
            return Center();
          }

          return Consumer<OrderProvider>(
              builder: (ctx, orderData, child) => ListView.builder(
                itemBuilder: (ctx, i) {
                  return OrderListItem(order: orderData.orderList[i]);
                },
                itemCount: orderData.orderList.length,
              ));
        },
      ),
    );
  }
}

