import 'package:alphazone_shop/provider/product_list.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProductDetailScreenArguments {
  final int productId;

  ProductDetailScreenArguments({required this.productId});
}

class ProductDetailScreen extends StatelessWidget {
  static const routeName = "/product-detail";

  const ProductDetailScreen({Key? key}) : super(key: key);

  Widget get productNotFound {
    return Scaffold(
      appBar: AppBar(
        title: Text("Product not found"),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)?.settings.arguments;

    if (args == null) {
      return productNotFound;
    }
    final productId = (args as ProductDetailScreenArguments).productId;

    final selectedProduct =
        Provider.of<ProductListProvider>(context, listen: false)
            .itemList
            .firstWhere((element) => element.id == productId);

    return Scaffold(
      appBar: AppBar(
        title: Text(selectedProduct.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 300,
              child: Image.network(selectedProduct.imageUrl,
                  width: double.infinity, fit: BoxFit.cover),
            ),
            SizedBox(height: 10),
            Text('\$${selectedProduct.price}',
                style: TextStyle(color: Colors.grey, fontSize: 20)),
            SizedBox(height: 10),
            Container(
                width: double.infinity,
                child: Text(
                  selectedProduct.description,
                  textAlign: TextAlign.center,
                  softWrap: true,
                )),
          ],
        ),
      ),
    );
  }
}
