import 'dart:convert';

import 'package:alphazone_shop/provider/product.dart';

String getEncodedProduct (ProductProvider product) {
  return jsonEncode({
    "title": product.title,
    "description": product.description,
    "price": product.price,
    "image_url": product.imageUrl,
  });
}