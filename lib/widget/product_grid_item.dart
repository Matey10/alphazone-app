import 'package:alphazone_shop/provider/cart.dart';
import 'package:alphazone_shop/provider/product.dart';
import 'package:alphazone_shop/provider/product_list.dart';
import 'package:alphazone_shop/screen/product_detail.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProductGridItem extends StatelessWidget {
  final bool isFavorite;

  const ProductGridItem({required this.isFavorite, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final product = Provider.of<ProductProvider>(context, listen: false);
    final cart = Provider.of<CartProvider>(context, listen: false);

    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).pushNamed(ProductDetailScreen.routeName,
              arguments: ProductDetailScreenArguments(productId: product.id));
        },
        child: GridTile(
          child: Image.network(
            product.imageUrl,
            fit: BoxFit.cover,
          ),
          footer: Container(
            decoration: BoxDecoration(color: Colors.white38),
            child: Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          product.title,
                        ),
                        Text(
                          "\$${product.price}",
                          textAlign: TextAlign.left,
                        ),
                      ],
                    ),
                  ),
                ),
                Consumer<ProductProvider>(
                  builder: (ctx, product, child) => IconButton(
                    icon: Icon(
                        isFavorite ? Icons.favorite : Icons.favorite_border,
                        color: isFavorite
                            ? Theme.of(context).accentColor
                            : Theme.of(context).textTheme.bodyText1?.color),
                    onPressed: () {
                      Provider.of<ProductListProvider>(context, listen: false)
                          .toggleFavorite(product.id);
                    },
                  ),
                ),
                IconButton(
                    icon: Icon(Icons.shopping_cart_outlined),
                    onPressed: () {
                      cart.addItem(product);
                      ScaffoldMessenger.of(context).hideCurrentSnackBar();
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text('Added to card'),
                        action: SnackBarAction(
                          label: "UNDO",
                          onPressed: () {
                            cart.removeSingleItem(product.id);
                          },
                        ),
                        duration: Duration(seconds: 2),
                      ));
                    }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
