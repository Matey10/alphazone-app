import 'dart:convert';
import 'dart:io';

import 'package:alphazone_shop/model/http_exception.dart';
import 'package:http/http.dart';

const headers = {"Content-Type": "application/json"};
const baseUrl = "172.20.10.3:8888";

getHeadersWithAuth (String token) {
  final headersWithAuth = {...headers};

  headersWithAuth["Authorization"] = "bearer $token";

  return headersWithAuth;
}

Future<Response> handleHttpError (Response resp) {
  if (resp.statusCode != HttpStatus.ok) {
    return Future.error(HttpException.fromJson(jsonDecode(resp.body)));
  }

  return Future.value(resp);
}