import 'package:alphazone_shop/provider/cart.dart';
import 'package:alphazone_shop/provider/product_list.dart';
import 'package:alphazone_shop/screen/cart.dart';
import 'package:alphazone_shop/widget/app_drawer.dart';
import 'package:alphazone_shop/widget/product_grid.dart';
import 'package:alphazone_shop/widget/badge.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

enum FilterOption {
  Favorites,
  All,
}

class ProductOverviewScreen extends StatefulWidget {
  static const routeName = "/product-overview";

  @override
  _ProductOverviewScreenState createState() => _ProductOverviewScreenState();
}

class _ProductOverviewScreenState extends State<ProductOverviewScreen> {
  bool _showOnlyFavorite = false;
  bool _isLoading = true;

  @override
  void initState() {
    Provider.of<ProductListProvider>(context, listen: false).fetchProductList()
        .then((value) => {
      setState(() {
        _isLoading = false;
      })
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Alphazone Shop'),
          actions: [
            Consumer<ProductListProvider>(
              builder: (ctx, provider, child) => PopupMenuButton(
                icon: Icon(Icons.more_vert),
                onSelected: (FilterOption selectedValue) {
                  setState(() {
                    if (selectedValue == FilterOption.Favorites) {
                      _showOnlyFavorite = true;
                    } else {
                      _showOnlyFavorite = false;
                    }
                  });
                },
                itemBuilder: (_) => [
                  PopupMenuItem(
                      child: Text('Only Favorites'),
                      value: FilterOption.Favorites),
                  PopupMenuItem(
                      child: Text('Show all'), value: FilterOption.All),
                ],
              ),
            ),
            Consumer<CartProvider>(
              builder: (_, cart, child) {
                return Badge(
                  child: child != null ? child : Container(),
                  value: cart.itemCount.toString(),
                );
              },
              child: IconButton(
                  icon: Icon(Icons.shopping_cart),
                  onPressed: () {
                    Navigator.of(context).pushNamed(CartScreen.routeName);
                  }),
            )
          ],
        ),
        drawer: AppDrawer(),
        body: _isLoading ? Center(child: CircularProgressIndicator(),) : ProductGrid(onlyFavorite: _showOnlyFavorite));
  }
}
