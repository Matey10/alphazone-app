import 'dart:async';

class HttpException implements Exception {
  final String message;
  final String code;
  final int statusCode;

  HttpException({ required this.message, required this.code, required this.statusCode });

  @override
  String toString() {
    return message;
  }

  HttpException.fromJson(Map<String, dynamic> json)
      : message = json['message'],
        code = json['code'],
        statusCode = json['status_code'];
}

typedef FutureOr<dynamic> UnauthorizedHandler(Object);