import 'package:alphazone_shop/provider/cart.dart';
import 'package:alphazone_shop/provider/order.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OrderButton extends StatefulWidget {
  final CartProvider cart;

  const OrderButton({Key? key, required this.cart}) : super(key: key);

  @override
  _OrderButtonState createState() => _OrderButtonState();
}

class _OrderButtonState extends State<OrderButton> {
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: (widget.cart.totalAmount <= 0 || _isLoading) ? null : () async {
        setState(() {
          _isLoading = true;
        });
        try {
          await Provider.of<OrderProvider>(context, listen: false).addOrder(
              widget.cart.itemMap.values.toList(),
              widget.cart.totalAmount
          );
          widget.cart.clear();
        } finally  {
          setState(() {
            _isLoading = false;
          });
        }
      },
      child: _isLoading ? CircularProgressIndicator() : Text('ORDER NOW'),
      style: ButtonStyle(
          foregroundColor: MaterialStateProperty.all(
              Theme.of(context).primaryColor)),
    );
  }
}
