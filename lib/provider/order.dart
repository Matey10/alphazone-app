import 'package:alphazone_shop/api/order.dart';
import 'package:alphazone_shop/model/http_exception.dart';
import 'package:alphazone_shop/model/order.dart';
import 'package:alphazone_shop/provider/cart.dart';
import 'package:alphazone_shop/utils/auth.dart';
import 'package:flutter/material.dart';

class OrderProvider with ChangeNotifier {
  final String token;
  final UnauthorizedHandler onUnauthorized;
  List<Order> _orderList = [];

  OrderProvider({token, List<Order>? orderList, onUnauthorized})
      : this.token = token == null ? '' : token,
        this._orderList = orderList == null ? [] : orderList,
  this.onUnauthorized =
  onUnauthorized == null ? defaultUnauthorized : onUnauthorized;

  List<Order> get orderList {
    return [..._orderList];
  }

  Future<void> fetchOrderList () async {
    try {
      var list = await getOrderList(token: token);

      _orderList = list.reversed.toList();
    } catch (error) {
      onUnauthorized(error);
    }

    notifyListeners();
  }

  Future<void> addOrder(List<OrderItem> cartItemList, double total) {
    return createOrder(token: token, order: Order(
        id: 0,
        cartItemList: cartItemList,
        amount: total,
        dateTime: 0)).then((order) {
          _orderList.add(order);
        notifyListeners();
    });
  }
}
