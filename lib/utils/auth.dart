import 'dart:async';

FutureOr<dynamic> defaultUnauthorized(err) {
  print("Unhandled unauthorized");

  return Future.error(err);
}