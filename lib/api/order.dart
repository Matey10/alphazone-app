import 'dart:convert';

import 'package:alphazone_shop/api/main.dart';
import 'package:alphazone_shop/model/order.dart';
import 'package:http/http.dart';

Future<Order> createOrder({required Order order, required String token}) async {
  final url = Uri.http(baseUrl, '/order');

  return post(url, headers: getHeadersWithAuth(token), body: jsonEncode(order))
    .then(handleHttpError)
    .then((resp) => Order.fromJson(jsonDecode(resp.body)));
}

Future<List<Order>> getOrderList({ required String token }) async {
  final url = Uri.http(baseUrl, '/order/list');

  return get(url, headers: getHeadersWithAuth(token)).then(handleHttpError).then((resp) {
    Iterable respList = json.decode(resp.body);
    return List<Order>.from(
        respList.map((model) => Order.fromJson(model))).toList();
  });
}