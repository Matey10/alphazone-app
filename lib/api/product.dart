import 'dart:convert';

import 'package:alphazone_shop/api/main.dart';
import 'package:alphazone_shop/provider/product.dart';
import 'package:http/http.dart';

Future<ProductProvider> createProduct({
  required ProductProvider product,
  required String token,
}) {
  final url = Uri.http(baseUrl, '/user-product');
  return post(url,
          headers: getHeadersWithAuth(token), body: jsonEncode(product))
      .then(handleHttpError)
      .then((resp) => ProductProvider.fromJson(jsonDecode(resp.body)));
}

Future<List<ProductProvider>> getProductList() {
  final url = Uri.http(baseUrl, '/product/list');
  return get(url, headers: headers).then(handleHttpError).then((resp) {
    Iterable respList = json.decode(resp.body);

    return List<ProductProvider>.from(
        respList.map((model) => ProductProvider.fromJson(model))).toList();
  });
}

Future<List<ProductProvider>> getUserProductList({ required String token }) {
  final url = Uri.http(baseUrl, '/user-product');
  return get(url, headers: getHeadersWithAuth(token)).then(handleHttpError).then((resp) {
    Iterable respList = json.decode(resp.body);

    return List<ProductProvider>.from(
        respList.map((model) => ProductProvider.fromJson(model))).toList();
  });
}

Future<List<int>> getFavoriteProductIdList(String token) {
  final url = Uri.http(baseUrl, '/user-product/favorite-list');

  return get(url, headers: getHeadersWithAuth(token))
      .then(handleHttpError)
      .then((resp) {
    Iterable respList = json.decode(resp.body);

    return List<int>.from(respList).toList();
  });
}

Future<dynamic> favoriteProduct({required int id, required String token}) {
  final url = Uri.http(baseUrl, '/user-product/favorite-list/add');
  final Map<String, dynamic> request = { "id": id };

  return post(url,
      headers: getHeadersWithAuth(token),
      body: jsonEncode(request)).then(handleHttpError);
}

Future<dynamic> unfavoriteProduct({required int id, required String token}) {
  final url = Uri.http(baseUrl, '/user-product/favorite-list/remove');
  final Map<String, dynamic> request = { "id": id };

  return post(url,
      headers: getHeadersWithAuth(token),
      body: jsonEncode(request)).then(handleHttpError);
}

Future<ProductProvider> updateProduct({
  required ProductProvider product,
  required String token,
}) {
  final url = Uri.http(baseUrl, '/user-product');

  return put(url, headers: getHeadersWithAuth(token), body: jsonEncode(product))
      .then(handleHttpError)
      .then((resp) => ProductProvider.fromJson(jsonDecode(resp.body)));
}

Future deleteProduct({required int id, required String token}) {
  final url = Uri.http(baseUrl, '/user-product/$id');

  return delete(url, headers: getHeadersWithAuth(token))
      .then(handleHttpError)
      .then((_) => null);
}
