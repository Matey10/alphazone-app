import 'package:alphazone_shop/provider/product.dart';
import 'package:alphazone_shop/screen/edit_product.dart';
import 'package:flutter/material.dart';

class UserProductListItem extends StatelessWidget {
  final ProductProvider product;
  final void Function(int id) onDelete;

  const UserProductListItem({Key? key, required this.product, required this.onDelete})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(product.title),
      leading: CircleAvatar(
        backgroundImage: NetworkImage(product.imageUrl),
      ),
      trailing: Container(
        width: 100,
        child: Row(
          // mainAxisSize: MainAxisSize.min,
          children: [
            IconButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(EditProductScreen.routeName,
                      arguments: product.id);
                },
                icon: Icon(Icons.edit),
                color: Theme.of(context).primaryColor),
            IconButton(
              onPressed: () {
                final id = product.id;

                if (id == null) {
                  return;
                }

                onDelete(id);
              },
              icon: Icon(Icons.delete),
              color: Theme.of(context).errorColor,
            ),
          ],
        ),
      ),
    );
  }
}
