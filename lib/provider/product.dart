import 'package:flutter/foundation.dart';

class ProductProvider with ChangeNotifier {
  final int id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;

  ProductProvider({
    required this.id,
    required this.title,
    required this.description,
    required this.price,
    required this.imageUrl,
  });

  ProductProvider.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        title = json['title'],
        description = json['description'],
        price = double.parse(json['price'].toString()),
        imageUrl = json['image_url'];

  Map<String, dynamic> toJson() => {
    'id': id,
    'title': title,
    'description': description,
    'price': price,
    'image_url': imageUrl,
  };
}