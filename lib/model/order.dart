import 'dart:convert';

import 'package:alphazone_shop/provider/cart.dart';

class Order {
  final int id;
  final double amount;
  final List<OrderItem> cartItemList;
  final int dateTime;

  Order(
      {required this.id,
      required this.amount,
      required this.cartItemList,
      required this.dateTime});

  Order.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        amount = json['amount'],
        dateTime = json['date_time'],
        cartItemList = json['items'].map<OrderItem>((item) {
          return OrderItem.fromJson(new Map<String, dynamic>.from(item));
        }).toList();

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'amount': amount,
      'items': cartItemList,
      'dateTime': dateTime
    };
  }
}
