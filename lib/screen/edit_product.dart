import 'package:alphazone_shop/provider/auth.dart';
import 'package:alphazone_shop/provider/product.dart';
import 'package:alphazone_shop/provider/product_list.dart';
import 'package:alphazone_shop/utils/auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EditProductScreen extends StatefulWidget {
  static const routeName = 'edit-product';

  const EditProductScreen({Key? key}) : super(key: key);

  @override
  _EditProductScreenState createState() => _EditProductScreenState();
}

class _EditProductScreenState extends State<EditProductScreen> {
  final _priceFocusNode = FocusNode();
  final _descriptionFocusNode = FocusNode();
  final _imageUrlController = TextEditingController();
  final _imageUrlFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();

  bool _isInit = false;
  bool _isLoading = false;

  var _editedProduct = ProductProvider(
      id: 0, title: '', description: '', price: 0, imageUrl: '');

  @override
  void dispose() {
    _priceFocusNode.dispose();
    _descriptionFocusNode.dispose();
    _imageUrlController.dispose();
    _imageUrlFocusNode.removeListener(_updateImageUrl);
    _imageUrlFocusNode.dispose();

    super.dispose();
  }

  void _updateImageUrl() {
    if (!_imageUrlFocusNode.hasFocus) {
      final value = _imageUrlController.text;

      if (value.isEmpty ||
          !value.startsWith("http") ||
          (!value.endsWith('.png') && !value.endsWith('.jpg'))) {
        return;
      }

      setState(() {});
    }
  }

  @override
  void initState() {
    _imageUrlFocusNode.addListener(_updateImageUrl);

    // TODO: implement initState
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_isInit) {
      final navigationContext = ModalRoute.of(context);

      if (navigationContext != null) {
        final arguments = navigationContext.settings.arguments;

        if (arguments != null) {
          final productId = navigationContext.settings.arguments as int;

          _editedProduct =
              Provider.of<ProductListProvider>(context).findById(productId);

          _imageUrlController.text = _editedProduct.imageUrl;
        }
      }

      _isInit = false;
    }

    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  void _saveForm() {
    final fState = _form.currentState;

    if (fState == null) {
      return;
    }

    final isValid = fState.validate();

    if (!isValid) {
      return;
    }

    fState.save();

    setState(() {
      _isLoading = true;
    });

    if (_editedProduct.id == 0) {
      Provider.of<ProductListProvider>(context, listen: false)
          .addProduct(product: _editedProduct)
          .catchError((err) {
        return showDialog<Null>(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('An error occurred'),
            content: Text(err.toString()),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Okay'))
            ],
          ),
        );
      }).then((_) {
        setState(() {
          _isLoading = false;
          Navigator.of(context).pop();
        });
      });
    } else {
      Provider.of<ProductListProvider>(context, listen: false)
          .updateProduct(product: _editedProduct)
          .catchError((err) {
        return showDialog<Null>(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text('An error occurred'),
            content: Text(err.toString()),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Okay'))
            ],
          ),
        );
      }).then((_) {
        setState(() {
          _isLoading = false;
          Navigator.of(context).pop();
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Edit Product'),
          actions: [
            IconButton(onPressed: _saveForm, icon: Icon(Icons.save)),
          ],
        ),
        body: _isLoading
            ? Center(child: CircularProgressIndicator())
            : Padding(
                padding: const EdgeInsets.all(16.0),
                child: Form(
                  key: _form,
                  child: ListView(
                    children: [
                      TextFormField(
                        initialValue: _editedProduct.title,
                        decoration: InputDecoration(labelText: 'Title'),
                        textInputAction: TextInputAction.next,
                        onFieldSubmitted: (_) {
                          FocusScope.of(context).requestFocus(_priceFocusNode);
                        },
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please provide a value.';
                          }

                          return null;
                        },
                        onSaved: (value) {
                          if (value == null) {
                            return;
                          }

                          _editedProduct = ProductProvider(
                            title: value,
                            id: _editedProduct.id,
                            description: _editedProduct.description,
                            imageUrl: _editedProduct.imageUrl,
                            price: _editedProduct.price,
                          );
                        },
                      ),
                      TextFormField(
                        initialValue: _editedProduct.price.toString(),
                        decoration: InputDecoration(labelText: 'Price'),
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.number,
                        focusNode: _priceFocusNode,
                        onFieldSubmitted: (_) {
                          FocusScope.of(context)
                              .requestFocus(_descriptionFocusNode);
                        },
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a price.';
                          }

                          final dValue = double.tryParse(value);
                          if (dValue == null) {
                            return 'Please enter a valid number';
                          }

                          if (dValue <= 0) {
                            return 'Please enter a number greater than zero.';
                          }

                          return null;
                        },
                        onSaved: (value) {
                          if (value == null) {
                            return;
                          }

                          _editedProduct = ProductProvider(
                            title: _editedProduct.title,
                            id: _editedProduct.id,
                            description: _editedProduct.description,
                            imageUrl: _editedProduct.imageUrl,
                            price: double.parse(value),
                          );
                        },
                      ),
                      TextFormField(
                        initialValue: _editedProduct.description,
                        decoration: InputDecoration(labelText: 'Description'),
                        maxLines: 3,
                        focusNode: _descriptionFocusNode,
                        keyboardType: TextInputType.multiline,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a description.';
                          }

                          if (value.length < 10) {
                            return 'Description is too short';
                          }

                          return null;
                        },
                        onSaved: (value) {
                          if (value == null) {
                            return;
                          }
                          _editedProduct = ProductProvider(
                            title: _editedProduct.title,
                            id: _editedProduct.id,
                            description: value,
                            imageUrl: _editedProduct.imageUrl,
                            price: _editedProduct.price,
                          );
                        },
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Container(
                            width: 100,
                            height: 100,
                            margin: EdgeInsets.only(top: 8, right: 10),
                            decoration: BoxDecoration(
                                border:
                                    Border.all(width: 1, color: Colors.grey)),
                            child: _imageUrlController.text.isEmpty
                                ? Text('Enter a URL')
                                : FittedBox(
                                    child: Image.network(
                                        _imageUrlController.text,
                                        fit: BoxFit.cover),
                                  ),
                          ),
                          Expanded(
                            child: TextFormField(
                                decoration:
                                    InputDecoration(labelText: 'Image URL'),
                                keyboardType: TextInputType.url,
                                textInputAction: TextInputAction.done,
                                controller: _imageUrlController,
                                onEditingComplete: () {
                                  setState(() {});
                                },
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Please enter an image url.';
                                  }

                                  if (!value.startsWith("http")) {
                                    return 'Please enter a valid url.';
                                  }

                                  if (!value.endsWith('.png') &&
                                      !value.endsWith('.jpg')) {
                                    return 'Please enter a valid url.';
                                  }

                                  return null;
                                },
                                onSaved: (value) {
                                  if (value == null) {
                                    return;
                                  }

                                  _editedProduct = ProductProvider(
                                    title: _editedProduct.title,
                                    id: _editedProduct.id,
                                    description: _editedProduct.description,
                                    imageUrl: value,
                                    price: _editedProduct.price,
                                  );
                                },
                                focusNode: _imageUrlFocusNode,
                                onFieldSubmitted: (value) {
                                  _saveForm();
                                }),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ));
  }
}
