import 'package:alphazone_shop/provider/product.dart';
import 'package:flutter/foundation.dart';

class OrderItem {
  final int id;
  final int productId;
  final ProductProvider product;
  final int quantity;

  OrderItem({
    required this.id,
    required this.product,
    required this.productId,
    required this.quantity,
  });

  Map<String, dynamic> toJson() => {
    'id': id,
    'product_id': productId,
    'product': product,
    'quantity': quantity
  };

  OrderItem.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        productId = json['product_id'],
        product = ProductProvider.fromJson(json['product']),
        quantity = json['quantity'];
}

class CartProvider with ChangeNotifier {
  Map<int, OrderItem> _itemMap = {};

  Map<int, OrderItem> get itemMap {
    return {..._itemMap};
  }

  int get itemCount {
    return _itemMap.entries.length;
  }

  double get totalAmount {
    return _itemMap.values.toList().fold(0.0,
        (previousValue, item) => item.product.price * item.quantity + previousValue);
  }

  void addItem(ProductProvider product) {
    if (_itemMap.containsKey(product.id)) {
      _itemMap.update(
          product.id,
          (existingCartItem) => OrderItem(
              id: existingCartItem.id,
              productId: product.id,
              product: existingCartItem.product,
              quantity: existingCartItem.quantity + 1,
          ));
    } else {
      _itemMap.putIfAbsent(
        product.id,
        () => OrderItem(
          id: DateTime.now().millisecondsSinceEpoch,
          product: product,
          productId: product.id,
          quantity: 1,
        ),
      );
    }

    notifyListeners();
  }

  void removeItem(int productId) {
    _itemMap.remove(productId);

    notifyListeners();
  }

  void removeSingleItem(int productId) {
    final item = itemMap[productId];

    if (item == null) {
      return;
    }

    if (item.quantity > 1) {
      _itemMap.update(
          productId,
          (existingCardItem) => OrderItem(
              id: existingCardItem.id,
              productId: existingCardItem.productId,
              product: existingCardItem.product,
              quantity: existingCardItem.quantity - 1,
          ));
    } else {
      removeItem(productId);
    }
    notifyListeners();
  }

  void clear() {
    _itemMap = {};
    notifyListeners();
  }
}
