class LoginResponse {
  String token;
  DateTime expiryDate;

  LoginResponse.fromJson(Map<String, dynamic> json)
      : token = json['token'],
        expiryDate = DateTime.fromMillisecondsSinceEpoch(json['expires_at'] * 1000);
}