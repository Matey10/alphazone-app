import 'dart:async';
import 'dart:io';

import 'package:alphazone_shop/api/auth.dart' as api;
import 'package:alphazone_shop/model/http_exception.dart';
import 'package:flutter/material.dart';

class AuthProvider with ChangeNotifier {
  String? _token;
  DateTime? _expiryDate;
  Timer? _autoLogoutTimer;

  bool get isAuth {
    return _token != null;
  }

  String get token {
    if (_token == null) {
      return '';
    }

    return _token!;
  }

  FutureOr<dynamic> handleUnauthorized(error) {
    if (error.runtimeType == HttpException) {
      if ((error as HttpException).statusCode == HttpStatus.unauthorized) {
        _token = null;
        _expiryDate = null;
        notifyListeners();
      }
    }

    return Future.error(error);
  }

  Future signUp(String email, String password) {
    return api.signUp(email, password).then((_) => login(email, password));
  }

  Future login(String email, String password) async {
    return api.login(email, password).then((response) {
      _token = response.token;
      _expiryDate = response.expiryDate;
      _autoLogout();

      notifyListeners();
    });
  }

  Future logout() async {
    return api.logout(token).then((_) {
      _token = null;
      _expiryDate = null;
      if (_autoLogoutTimer != null) {
        _autoLogoutTimer!.cancel();
        _autoLogoutTimer = null;
      }

      notifyListeners();
    });
  }

  void _autoLogout() {
    if (_autoLogoutTimer != null) {
      _autoLogoutTimer!.cancel();
    }

    final timeToExpiry = _expiryDate!.difference(DateTime.now()).inSeconds;
    _autoLogoutTimer = Timer(Duration(seconds: timeToExpiry), logout);
  }
}
